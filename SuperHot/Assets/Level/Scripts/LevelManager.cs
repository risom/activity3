﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {


	// drop your spawn points in this array in the inspector
	public GameObject[] spawnPoints;

	public GameObject NPC;

	// Loop through tiles on board/level and displace random game objects
	void Start () {

		// Grab Spawn points and out into array
		spawnPoints = GameObject.FindGameObjectsWithTag("floor");
		for(int i = 0; i < 3; i++){

			// *** doesnt fulfill requirements but get random tiles on board/level
			// rando - position from list of tile locations
			// ***
			int rando = Random.Range (0,spawnPoints.Length);
			Vector3 position = new Vector3 (); 
			position = spawnPoints [rando].transform.position;
		
			// *** tried to make NPC here, one of many iterations
			// Instantiate(NPC, position, Quaternion.identity);
			// ***
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.AddComponent<Rigidbody>();
			cube.tag = "npc";
			cube.transform.position = new Vector3 (position.x,position.y,position.z);
		}
		//Vector3 position = new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f));
		//Instantiate(prefab, position, Quaternion.identity);


	}
		
	// Update is called once per frame
	void Update () {
		
	}
 
}
