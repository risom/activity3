﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour {

	public KeyCode spaceBar = KeyCode.Space;
	public float fireRate = .25f;
	public float weaponRange = 100f;
	public Transform gunEnd;

	private Camera fpsCamera;
	private LineRenderer laserLine;
	private WaitForSeconds shotDuration = new WaitForSeconds (.07f);
	private float nextFire;

	// ***
	// Gets line and Camera
	// ***
	void Start () {

		laserLine = GetComponent<LineRenderer> ();
		fpsCamera = GetComponentInParent<Camera> ();

	}
	
	// **
	// Checks if user presses spacebar to fire laser
	// **
	void Update () {
		if (Input.GetKeyDown (spaceBar) && Time.time > nextFire) {
			nextFire = Time.time + fireRate;

			StartCoroutine (ShotEffect ());
		}
	}

	// enable shots and wait between
	private IEnumerator ShotEffect(){
	
		laserLine.enabled = true;
		yield return shotDuration;
		laserLine.enabled = false;
		Debug.Log("fired");
	
	}
}
